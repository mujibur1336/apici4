<?php
use App\Models\Model_otentikasi;
use Firebase\JWT\JWT;
use Firebase\JWT\Key; 

function getJWT($otentikasiHeader)
{
	if (is_null($otentikasiHeader)) {
		throw new Exception("Otentikasi JWT Gagal");
	}
	return explode(" ", $otentikasiHeader[1]);
}

function validateJWT($encodedToken)
{
	$key = getenv('JWT_SECRET_KEY');
	$decodedToken = JWT::decode($encodedToken, $key, ['HS256']);
	// $decodedToken = JWT::decode($encodedToken, new Key($key, 'HS256'));
	$Modelotentikasi = new Model_otentikasi();

	$Modelotentikasi->getEmail($decodedToken->email);
}

function createJWT($email){

	$waktuRequest = time();
	$waktuToken   = getenv('JWT_TIME_TO_LIVE');
	$wakuExpired  = $waktuRequest + $waktuToken;
	$payload = [

		'email' => $email,
		'iat'	=> $waktuRequest,
		'exp'	=> $wakuExpired
	];

	// $jwt = JWT::encode($payload, getenv('JWT_SECRET_KEY'));
	$jwt = JWT::encode($payload,getenv('JWT_SECRET_KEY'), 'HS256');
	return $jwt;
}