<?php

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;

class Model_pegawai extends Model
{
    protected $table = "pegawai";
    protected $primaryKey = "id";
    protected $allowedFields = ['nama', 'email'];

    protected $validationRules =[
    	'nama' => 'required',
    	'email' => 'required|valid_email',
    ];

    protected $validationMessages =[
    	'nama' => ['required' => 'Silahkan Masukan Nama anda'],
    	'email' => ['required' => 'Silahkan Masukan email anda', 'valid_email' => 'Email anda tidak valid'],
    ];
}