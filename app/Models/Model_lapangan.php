<?php

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;

class Model_lapangan extends Model
{
    protected $table = "lapangan";
    protected $primaryKey = "id";
    protected $allowedFields = ['nama', 'lokasi'];

    protected $validationRules =[
    	'nama' => 'required',
    	'lokasi' => 'required',
    ];

    protected $validationMessages =[
    	'nama' => ['required' => 'Silahkan Masukan Nama lapangan'],
    	'lokasi' => ['required' => 'Silahkan Masukan lokasi lapangan'],
    ];
}