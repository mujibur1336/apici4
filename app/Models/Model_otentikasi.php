<?php

namespace App\Models;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\Model;
use Exception;
class Model_otentikasi extends Model
{
    protected $table = "otentikasi";
    protected $primaryKey = "id";
    protected $allowedFields = ['email', 'password'];

    protected $validationRules =[
    	'email' => 'required|valid_email',
    	'password' => 'required',
    ];

function getEmail($email)
{
	$builder = $this->table("otentikasi");
	$data = $builder->where("email", $email)->first();
	if (!$data) {
		throw new Exception("Data otentikasi tidak ditemukan");		
	}

	return $data;
}
   
}