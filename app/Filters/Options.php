<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

use CodeIgniter\API\ResponseTrait;


Class Options implements FilterInterface
{
    use ResponseTrait;
    public function before(RequestInterface $request)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        
        
        if ( $request->getMethod() == 'options')
        {
            $response = service('response');
            $response->setJSON(['method' => 'OPTIONS']);
            return $response;
            die();
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response)
    {
      // Do something here
    }
}