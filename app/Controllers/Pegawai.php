<?php

namespace App\Controllers;
use Codeigniter\API\ResponseTrait;
use App\Models\Model_pegawai;

class Pegawai extends BaseController
{

    use ResponseTrait;
    function __construct(){
        $this->model = new Model_pegawai();
    }
    public function index()
    { 
        $data = $this->model->orderBy('nama', 'asc')->findAll();
        return $this->respond($data,200);
    }

    public function show($id = null)
    {
        $data = $this->model->where('id',$id)->findAll(); 
        if ($data) {
            return $this->respond($data,200);
        }else{
            return $this->failNotFound("Data tidak ada untuk id $id");
        }
    }

    public function create()
    {

        // $data = [
        //     'nama'=>$this->request->getVar('nama'),
        //     'email'=>$this->request->getVar('email')
        // ];
        $data = $this->request->getPost();
        // $this->model->save($data);
        if (!$this->model->save($data)) {
            return $this->fail($this->model->errors());
        }
        $response = [
            'status' => 201,
            'error'  => null,
            'messages' => [
                'success' => 'Data Berhasil Disimpan'
            ]
        ];

        return $this->respond($response);

    }

    public function update($id = null)
    {
        $data = $this->request->getRawInput();
        $data['id'] = $id;
        $isExists = $this->model->where('id', $id)->findAll();

        if (!$isExists) {
            return $this->failNotFound("Data tidak ditemukan untuk id $id");
        }

        if (!$this->model->save($data)) {
            return $this->fail($this->model->errors());
        }

        $response = [
            'status' => 201,
            'error'  => null,
            'messages' => [
                'success' => "Data Berhasil Diubah"
            ]
        ];
        return $this->respond($response);
    }

    public function delete($id = null)
    {
        $data = $this->model->where('id', $id)->findAll();
        if ($data) {
            $this->model->delete($id);
            $response = [
                    'status' => 201,
                    'error'  => null,
                    'messages' => [
                        'success' => 'Data Berhasil Dihapus'
                    ]
            ];

            return $this->respondDeleted($response);
        }else{
             return $this->failNotFound("Data tidak ditemukan untuk id $id");
        }
    }
}
