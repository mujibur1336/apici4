<?php

namespace App\Controllers;
use Codeigniter\API\ResponseTrait;
use App\Models\Model_otentikasi;

class Otentikasi extends BaseController
{
    use ResponseTrait;
    function __construct(){
        $this->model = new Model_otentikasi();
    }
    public function index()
    {
        $validation = \Config\Services::validation();
        $aturan = [
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'Silahkan Masukan email',
                    'valid_email' => 'Silahkan Masukan email valid'
                ]
                ],
                'password' => [
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Silahkan Masukan Password',                        
                    ]
                ],
        ];
        $validation->setRules($aturan);
        if(!$validation->withRequest($this->request)->run()){
            return $this->fail($validation->getErrors());
        }

        $model = new Model_otentikasi();

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $data = $model->getEmail($email);
        if($data['password'] != md5($password)){
            return $this->fail("Password salah");
        }
        helper('jwt');
        $response = [
            'message' => 'Otentikasi Sukses',
            'data' => $data,
            'access_token' => createJWT($email)
        ];

        return $this->respond($response);
    }
}
