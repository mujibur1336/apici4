<?php

namespace App\Controllers;
use Codeigniter\API\ResponseTrait;
use App\Models\Model_lapangan;
use App\Libraries\Sap;
use App\Libraries\Sap_connection;
use App\Libraries\Sap_function;
use App\Libraries\Sap_mso_prod_new;


class Lapangan extends BaseController
{

    use ResponseTrait;
    protected $lib1;
    function __construct(){
        $this->model = new Model_lapangan();        
        // $this->lib1 =  new Sap_connection();
    }
    public function index()
    { 
        // $this->lib1 = new SAPConnection();
        // $this->lib1->Connect();
        // if ( $this->lib1->GetStatus() == SAPRFC_OK)
        //      $this->lib1->Open();
        // if ( $this->lib1->GetStatus() != SAPRFC_OK) {
        //     $this->lib1->PrintStatus();
        //     exit;
        // }
        // $fce = $this->lib1->NewFunction("Z_ZCBW_COMPANY");
        // if ($fce == false) {
        //     $this->lib1->PrintStatus();
        //     exit;
        // }


        
        $authHeader = $this->request->getHeader("api-key");
        $authHeader = $authHeader->getValue();      
        $data       = $this->model->findAll();
        $authHeaderKey = '827ccb0eea8a706c4c34a16891f84e7b';

        if ($authHeader == $authHeaderKey) {
            $response = [
                    'status' => 200,
                    'error' => FALSE,
                    'messages' => 'Data Lapangan',
                    'data' => $data
                ];
                return $this->respond($response);
        }else{
             $response = [
                'status' => 401,
                'error' => TRUE,
                'messages' => 'Access denied'
            ];
            return $this->respond($response);
        }
    }

    public function show($id = null)
    {
        $authHeader = $this->request->getHeader("api-key");
        $authHeader = $authHeader->getValue();      
        $data = $this->model->where('id',$id)->findAll(); 
        $authHeaderKey = '827ccb0eea8a706c4c34a16891f84e7b';               

        if ($authHeader == $authHeaderKey) {
            if ($data) {
            $response = [
                    'status' => 200,
                    'error' => FALSE,
                    'messages' => 'Data Lapangan',
                    'data' => $data
                ];
                return $this->respond($response);
            }else{
                return $this->failNotFound("Data tidak ada untuk id $id");
            }            
        }else{
             $response = [
                'status' => 401,
                'error' => TRUE,
                'messages' => 'Access denied'
            ];
            return $this->respond($response);
        }
    }

    public function create()
    {

        // $data = [
        //     'nama'=>$this->request->getVar('nama'),
        //     'email'=>$this->request->getVar('email')
        // ];
        // $this->model->save($data);
        $authHeader = $this->request->getHeader("api-key");
        $authHeader = $authHeader->getValue();      
        $data = $this->request->getPost();
        $authHeaderKey = '827ccb0eea8a706c4c34a16891f84e7b';                  

         if ($authHeader == $authHeaderKey) {
           if (!$this->model->save($data)) {
            return $this->fail($this->model->errors());
                }
                $response = [
                    'status' => 201,
                    'error'  => null,
                    'messages' => [
                        'success' => 'Data Berhasil Disimpan'
                    ]
                ];
                return $this->respond($response);
        }else{
             $response = [
                'status' => 401,
                'error' => TRUE,
                'messages' => 'Access denied'
            ];
            return $this->respond($response);
        }

    }

    public function update($id = null)
    {
        $data = $this->request->getRawInput();
        $data['id'] = $id;
        $isExists = $this->model->where('id', $id)->findAll();

        if (!$isExists) {
            return $this->failNotFound("Data tidak ditemukan untuk id $id");
        }

        if (!$this->model->save($data)) {
            return $this->fail($this->model->errors());
        }

        $response = [
            'status' => 201,
            'error'  => null,
            'messages' => [
                'success' => "Data Berhasil Diubah"
            ]
        ];
        return $this->respond($response);
    }

    public function delete($id = null)
    {
        $data = $this->model->where('id', $id)->findAll();
        if ($data) {
            $this->model->delete($id);
            $response = [
                    'status' => 201,
                    'error'  => null,
                    'messages' => [
                        'success' => 'Data Berhasil Dihapus'
                    ]
            ];

            return $this->respondDeleted($response);
        }else{
             return $this->failNotFound("Data tidak ditemukan untuk id $id");
        }
    }
}
